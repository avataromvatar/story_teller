


import 'package:story_teller/node/basic_node.dart';

class StringCommandParser extends BasicNode<String, List<dynamic>> {
  List<String> _command_BE;
  List<String> _worker_BE;
  dynamic Function(String formula)? def_worker;
  late Map<String, dynamic Function(String formula)> _workers;
  Map<String, dynamic Function(String formula)> get workers => _workers;

  StringCommandParser(
      {List<String> command_BE = const ['[[', ']]'],
      List<String> worker_BE = const ['', ':'],
      Map<String, dynamic Function(String formula)>? workers})
      : _command_BE = command_BE,
        _worker_BE = worker_BE,
        super('') {
          if(workers!=null)
          _workers = Map<String, dynamic Function(String formula)>.from(workers);
          else
          _workers = Map<String, dynamic Function(String formula)>();
    setRules(_main_worker);
    
  }
  
  List<dynamic> _main_worker(String input, String last) {
    List<dynamic> retData = List<dynamic>.empty(growable: true);
    int indexCE = -1;
    int indexWorkerBegin = -1;
    int indexWorkerEnd = -1;

    var partArray = input.split(_command_BE[0]); //рзбиваем все по начальному символу команды
    
    for (var i = 0; i < partArray.length; i++) {
      indexCE = partArray[i].indexOf(_command_BE[1]);//конец команды
      if (indexCE != -1) {
        indexWorkerBegin =
            _worker_BE[0].length > 0 ? partArray[i].indexOf(_worker_BE[0]) : 0;
        indexWorkerEnd = partArray[i].indexOf(_worker_BE[1]);

        // String command = partArray[i].substring(0,partArray[i].indexOf(_command_BE[1]));
        String worker =
            partArray[i].substring(indexWorkerBegin, indexWorkerEnd);
        String formula = partArray[i].substring(indexWorkerEnd+_worker_BE[1].length, indexCE);
        if (_workers.containsKey(worker)) {
          var tmp = _workers[worker]!(formula);
          if(tmp != null)
          retData.add(tmp);
        }
        retData.add(def_worker!=null?def_worker?.call(partArray[i].substring(indexCE+_command_BE[1].length)):partArray[i]);
      } else
        retData.add(def_worker!=null?def_worker?.call(partArray[i]):partArray[i]);
    }

    return retData;
  }
}
