import 'package:flutter/material.dart';

abstract class ICharacter
{
  Image get portrait;
  String get name;
  // Character(this.name, this.portrait);
}