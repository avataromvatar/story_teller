


import 'package:flutter/material.dart';
import 'package:story_teller/narrator/characters.dart';
import 'package:story_teller/scp/string_command_parser.dart';


class Narrator extends StringCommandParser
{
  late Map<String,ICharacter> _characters;
  ICharacter? narrator;
  late TextStyle def_style;
  late TextAlign def_align;
  late TextStyle _text_style;
  late TextAlign _text_align; 
  Narrator({Map<String,ICharacter> ?characters}):super(command_BE: ['[[',']]'],worker_BE: ['',':'])
  {
    if(characters ==null)
    _characters = Map<String,ICharacter>();
    else
    _characters = Map<String,ICharacter>.from(characters);
    def_worker = _teller;
    workers['char'] = _narrator_set;
    workers['style'] = _style_set;

    def_style = TextStyle();
    def_align = TextAlign.left;
    _text_style = def_style;
    _text_align = def_align;
  }

  dynamic _style_set(String formula)
  {
    if(formula.isEmpty)
      _text_style = def_style;
    else
    {
      if(formula == 'bold')
      {
        _text_style = TextStyle(fontWeight: FontWeight.bold);
      }
    }  
    
    return null;
  }

  dynamic _teller(String formula)
  {
    if(narrator == null)
    return Text(formula,style: _text_style,textAlign: _text_align,);
    else
    {
      return ListTile(
        leading: narrator!.portrait,
        title: Text(narrator!.name,style: _text_style,textAlign: _text_align,),
        subtitle: Text(formula,style: _text_style,textAlign: _text_align,),  
      );
    }
  }

  dynamic _narrator_set(String formula)
  {
    if(formula.isEmpty)
      narrator =null;
    
      if(_characters.containsKey(formula))
        narrator = _characters[formula];
    
    return null;
  }

}