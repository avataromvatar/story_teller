import 'dart:async';

import 'package:rxdart/rxdart.dart';


class BasicNode<I, O> {
  I _curr;
  I get record =>_curr;
  final PublishSubject<I> _input;
  PublishSubject<I> get input =>_input;
  final PublishSubject<O> _output;
  PublishSubject<O> get output=>_output;
  O? Function(I input, I curr) ?_rules;
  late StreamSubscription<I> _localSubcriber;

  BasicNode(I initalData,{O? Function(I input, I curr) ?rules})
      : _input = PublishSubject<I>(),
        _output = PublishSubject<O>(),
        _rules = rules,_curr = initalData {
    _localSubcriber = _input.listen(_processing);
  }
  void setRules(O? Function(I input, I curr) rules)
  {
    _rules = rules;
  }
  void add(I input) {
    _input.add(input);
  }

  StreamSubscription<O> listen(Function(O) cb) {
    return _output.listen(cb);
  }

  void _processing(I input) {
    if (_rules == null) {
      if (I.runtimeType == O.runtimeType) {
        _curr = input;
        _output.add(input as O);
      }
    }
    else
    {
      var tmp = _rules!.call(input,_curr);
      if(tmp!=null)
      {
      _output.add(tmp);
      _curr = input;
      }
    }
  }
}
